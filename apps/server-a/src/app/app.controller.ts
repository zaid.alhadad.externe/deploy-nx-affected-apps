import { SampleServiceService } from '@deploy-nx-affected-apps/sample-lib';
import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly sampleService: SampleServiceService
  ) {}

  @Get()
  getData() {
    console.log(this.sampleService.getZlib());

    return this.sampleService.getZlib();
  }
}
