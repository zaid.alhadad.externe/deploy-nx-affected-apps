import flow from 'lodash/flow';
import fromPairs from 'lodash/fromPairs';
import toPairs from 'lodash/toPairs';
import { stringify } from 'querystring';
// import { constants, gzipSync } from 'zlib';
import * as zlib from 'zlib';
const alignZipped = (compressed) => {
  // Override the GZip header containing the OS to always be Linux, so it will produce same result on any machine
  compressed[9] = 0x03;
  return compressed;
};

const gZipOptions = {
  level: zlib.constants.Z_BEST_COMPRESSION,
  memLevel: 9,
  strategy: zlib.constants.Z_DEFAULT_STRATEGY,
  windowBits: 15,
  chunckSize: 65536,
};

const encodeWhole = (content) => {
  return alignZipped(zlib.gzipSync(content, gZipOptions)).toString('base64');
};

const encodePairs = (pairs) => {
  return pairs.map((pair) => {
    const [key, value] = pair;

    const encodedKey = alignZipped(
      zlib.gzipSync(JSON.stringify(key), gZipOptions)
    ).toString('base64');
    const encodedValue = alignZipped(
      zlib.gzipSync(JSON.stringify(value), gZipOptions)
    ).toString('base64');

    return [encodedKey, encodedValue];
  });
};

const b = { b: 'ask', c: 'me', d: 'anything' };
export const bodyEncoded = flow(
  toPairs,
  encodePairs,
  fromPairs,
  stringify,
  encodeWhole
)(b);
export const bodyDecoded = 'b=ask&c=me&d=anything';

export const jsonBodyEncoded =
  '{"ry":"Imh0dHBzJTNBJTJGJTJGaGVseXN6aW5rZXJlc28uaHUlMkZ0ZXRzemlrLWxpc3RhJTNGZmlsdGVyJTVCY2F0ZWdvcnklNUQlM0QxIg==","nk":"Imh0dHBzJTNBJTJGJTJGaGVseXN6aW5rZXJlc28uaHUlMkZ0ZXRzemlrLWxpc3RhJTNGIg==","ev":"Imh0dHBzJTNBJTJGJTJGaGVseXN6aW5rZXJlc28uaHUlMkZ0ZXRzemlrLWxpc3RhJTNGIg==","pp":"Imh0dHBzJTNBJTJGJTJGaGVseXN6aW5rZXJlc28uaHUlMkZ0ZXRzemlrLWxpc3RhJTNGIg==","gun":"WyIyNDIiXQ==","pe":"ImFqYXgi","ex":"WyJleHRoZWFkIl0=","zx":"ImphdmFzY3JpcHQlM0F2b2lkKDApIg==","clock":"IjE2MDI0MTU0MTUxMzQi","ch":"Ijci","o":"ImFiMDUwMGM0MyI=","gu":"IjdxZW1iaGdxa2l0YTY0ZmZwM2RjampzYmkwMyI=","t":"IjYi","d":"IjIxIg==","col":"IjEi","oo":"IjguMC4xMyI=","pay":"IkFBRUFBQUFBQUF3UkR3QWtJUUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUElM0Qi"}';

export const jsonBodyDecoded =
  'ry=https%3A%2F%2Fhelyszinkereso.hu%2Ftetszik-lista%3Ffilter%5Bcategory%5D%3D1&nk=https%3A%2F%2Fhelyszinkereso.hu%2Ftetszik-lista%3F&ev=https%3A%2F%2Fhelyszinkereso.hu%2Ftetszik-lista%3F&pp=https%3A%2F%2Fhelyszinkereso.hu%2Ftetszik-lista%3F&gun=242&pe=ajax&ex=exthead&zx=javascript%3Avoid(0)&clock=1602415415134&ch=7&o=ab0500c43&gu=7qembhgqkita64ffp3dcjjsbi03&t=6&d=21&col=1&oo=8.0.13&pay=AAEAAAAAAAwRDwAkIQAAAAAAAAAAAAAAAAAAAAAAAAA%3D';
